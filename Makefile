
SHELL = /bin/bash
ENV_NAME = scb
HOSTNAME = $(shell hostname)
ACTIVATE = eval "$$(conda shell.bash hook)" && conda activate ${ENV_NAME}

REMOTE = embl
REMOTE_DIR = /g/mazimmer/bartmans/Projects/BB010_Steroids/
SYNC_FLAGS = -av
SYNC = rsync

.PHONY: pull_% push_% git_update install_precommit

ifeq (,$(findstring cluster.embl.de,${HOSTNAME}))
    REMOTE_DIR := ${REMOTE}:${REMOTE_DIR}
endif

test:
	echo ${HOSTNAME}

pull_%:
	${SYNC} ${SYNC_FLAGS} ${REMOTE_DIR}$* ./

push_%: SYNC_FLAGS += --delete
push_%:
	${SYNC} ${SYNC_FLAGS} ./$* ${REMOTE_DIR}

test_%: SYNC_FLAGS += -n
test_%: %
	echo $@

git_update:
	cd ${REMOTE_DIR} && git pull
		    
conda_env.yml:
	conda env export -n ${ENV_NAME} -f $@

conda_env_simple.yml: conda_env.yml
	conda env export -n ${ENV_NAME} -f $@ --from-history
	if grep -q "pip:" $^; then sed -i -e '$$d' $@ && sed -n -e '/pip:/,$$p' $^ >> $@; fi

.git/hooks/pre-commit:
	ln -s ../../.pre-commit $@

install_precommit: .git/hooks/pre-commit

