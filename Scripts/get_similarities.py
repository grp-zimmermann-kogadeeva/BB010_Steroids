#!/usr/bin/env python

import numpy as np
import pandas as pd
from scipy.stats import binned_statistic
from sklearn.metrics.pairwise import cosine_similarity


def get_binned_ms(ms_data, bin_size=0.01, min_mz=50, max_mz=1500):
    params = {"statistic": "mean", 
              "bins": np.arange(min_mz, max_mz+bin_size, bin_size), 
              "range": [min_mz, max_mz]}
    return np.nan_to_num(binned_statistic(*ms_data, **params)[0])

def main(exp_filename, pred_filename, out_filename, exp_col=15, pred_col=9):
    """
    exp_filename - path to table with experimental spectra
    pred_filename - path to table with predicted spectra
    out_filename - path to output table
    exp_col - index of the column in exp. table where spectra begin
    pred_col - index of the column in pred. table where spectra begin
    """
    df_exp = pd.read_excel(exp_filename)
    df_pred = pd.read_excel(pred_filename)

    binned_exp = [get_binned_ms([x, y]) for x, y 
                  in zip(df_exp.iloc[::2, exp_col:].values, 
                      df_exp.iloc[1::2, exp_col:].values)]

    binned_pred = [get_binned_ms([x, y]) for x, y in 
                   zip(df_pred.iloc[::2, pred_col:].values, 
                       df_pred.iloc[1::2, pred_col:].values)]

    sims = [cosine_similarity([x], [y])[0][0] for x, y 
            in zip(binned_exp, binned_pred)]

    df_new = df_pred.iloc[::2, :pred_col-1].copy()
    df_new["Cosine similarities"] = sims
    df_new.to_csv(out_filename, index=False)


if __name__ == "__main__":
    main("Data/Exp_ordered.xlsx",
         "Data/Predfilt_ordered.xlsx", 
         "Output/cos_similarities.csv")
